#!/bin/bash

#     _____                _              _
#    / ____|              | |            | |
#   | |     ___  _ __  ___| |_ __ _ _ __ | |_ ___
#   | |    / _ \| '_ \/ __| __/ _` | '_ \| __/ __|
#   | |___| (_) | | | \__ \ || (_| | | | | |_\__ \
#    \_____\___/|_| |_|___/\__\__,_|_| |_|\__|___/

GREEN="\033[32;1m"
NORMAL="\033[0m"
FLAG=$1
NETWORK=$2

#    ______                _   _
#   |  ____|              | | (_)
#   | |__ _   _ _ __   ___| |_ _  ___  _ __  ___
#   |  __| | | | '_ \ / __| __| |/ _ \| '_ \/ __|
#   | |  | |_| | | | | (__| |_| | (_) | | | \__ \
#   |_|   \__,_|_| |_|\___|\__|_|\___/|_| |_|___/
#
# ascii art: http://www.patorjk.com/software/taag/#p=display&c=bash&f=Big&t=Functions

function .print_system_info () {
    echo -e ">>>>>>>>>>>>>>>>>>>>${GREEN}System Information:${NORMAL}<<<<<<<<<<<<<<<<<<<<\n"
    hostnamectl
    echo "-------------------------------------------------------------"
    echo -e "\n\tCore Numbers:\t  $(grep -c 'processor' /proc/cpuinfo)"
    echo -e "\tCPU Model:\t $(cat <  /proc/cpuinfo | sed '5!d' | cut -d: -f2)"
    echo -e "\n>>>>>>>>>>>>>>>>>>>>${GREEN}End of System information${NORMAL}<<<<<<<<<<<<<<<<<<<<\n"
    exit 0
}

function .help() {
    cat <<END
        Avaiable Parameters
        -s Print System information
        -c Check For nmap install
        -a  <network>  Check active ips
        -p  <network> Check Open Ports
        -h Help
END
    exit 0
}

function .validate_nmap () {
    echo "Checking for nmap"
    _nmap_check=$(which nmap)
    #check if nmap is installed
    if  [[ -z $_nmap_check  ]]; then
        echo "Nmap not found. Installation ongoing. Please wait..."
        #helper variables to try find which package manager is installed
        _yum_cmd=$(which dnf)
        _apt_cmd=$(which apt-get)
       # validate the previous variables to check the package manager that we will use
        if [[ -n  $_yum_cmd ]]; then
            sudo dnf install -y  nmap
        elif [[ -n $_apt_cmd ]]; then
             sudo apt-get install -y  nmap
        else
            echo "error can't install nmap"
            exit 1;
        fi
    fi
    echo    "Nmap is installed"
    exit 0 # 0 == success; otherwise == some error
}

function .list_active_ips () {
    echo "Checking for active ips"
    if  [[ -z "$NETWORK" ]]; then
        echo "the correct usage is: nmap.sh -a 192.168.1.0/24 "
         exit 1
    else
         nmap -sn "$NETWORK"
    fi
    exit 0
}

function .list_open_ports () {
    echo "Checking for active ports"
    if  [[ -z "$NETWORK" ]]; then
        echo "the correct usage is: nmap.sh -p 192.168.1.0/24 "
        exit 1
    else
         nmap --top-ports 50 "$NETWORK"
    fi
    exit 0
}

#    __  __       _
#   |  \/  |     (_)
#   | \  / | __ _ _ _ __
#   | |\/| |/ _` | | '_ \
#   | |  | | (_| | | | | |
#   |_|  |_|\__,_|_|_| |_|
#
# ascii art: http://www.patorjk.com/software/taag/#p=display&c=bash&f=Big&t=Main
while :
    do
        case "$FLAG" in
            -s)  .print_system_info; break;;
            -c)  .validate_nmap; break ;;
            -a)  .list_active_ips; break ;;
            -p)  .list_open_ports; break;;
            -h)  .help; break;;
            -*) echo "bad argument $FLAG"; exit 1;;
            *)  .help; break;;
        esac
        shift
    done
